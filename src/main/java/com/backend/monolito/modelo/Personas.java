package com.backend.monolito.modelo;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.*;

import javax.persistence.*;
import java.util.List;

@Setter
@Getter
@Data
@Entity
@Table(name = Personas.TABLE_NAME)
public class Personas {
        public static final String TABLE_NAME = "personas";
        /*
        @id para identificar la llave primaria
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        */
        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Long id_persona;

        //@Column nombre de la columna en base de datos

        @Column(name = "cedula")
        private Long cedula;

        @Column(name = "nombres")
        private String nombres;

        @Column(name = "apellidos")
        private String apellidos;

        @OneToMany(
                mappedBy = "persona",
                cascade = CascadeType.ALL,
                orphanRemoval = false
        )
        @JsonIgnoreProperties("persona")
        @JsonBackReference
        private List <Imagenes> imagenes;

        public Personas(Long id_persona, Long cedula, String nombres, String apellidos) {
                this.id_persona = id_persona;
                this.cedula = cedula;
                this.nombres = nombres;
                this.apellidos = apellidos;
                //this.imagenes = imagenes;
        }

        public Personas() {

        }
}
