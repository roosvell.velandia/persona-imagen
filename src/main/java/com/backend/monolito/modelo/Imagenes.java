package com.backend.monolito.modelo;

import lombok.*;

import javax.persistence.*;

@Setter
@Getter
@Data
@Entity
@Table(name = Imagenes.TABLE_NAME)
public class Imagenes {
    public static final String TABLE_NAME = "imagenes";
    /*
        @id para identificar la llave primaria
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id_imagen;

    @Column(name = "imagen")
    private String imagen;

    /*@ManyToOne  hace referencia la relacion muchos a uno en este caso muchas personas
     * tienen una imagen
     * @JoinColumn  el campo que hace de referecia a la llave foranea
     * */
    @ManyToOne
    @JoinColumn(name = "id_persona")
    private Personas persona;

}
