package com.backend.monolito.modelo;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.CascadeType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;

@Setter
@Getter
@Data
@Document(collection = "imagenes_MySQL")
public class Imagenes_mongodb_MySQL {
    @Id
    private String _id;
    private String imagen;

    @ManyToOne
    @JoinColumn(name = "id_persona")
    private Personas persona;

}
