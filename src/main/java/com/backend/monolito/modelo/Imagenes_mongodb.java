package com.backend.monolito.modelo;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.*;

@Setter
@Getter
@Data
@Document(collection = "imagenes")
public class Imagenes_mongodb {
    @Id
    private String _id;
    private String imagen;
}
