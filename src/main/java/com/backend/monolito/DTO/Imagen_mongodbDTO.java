package com.backend.monolito.DTO;

import lombok.*;

@Setter
@Getter
@Data
public class Imagen_mongodbDTO {
    private String _id;
    private String imagen;
}
