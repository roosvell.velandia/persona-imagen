package com.backend.monolito.DTO;

import com.backend.monolito.modelo.Personas;
import lombok.*;

@Setter
@Getter
@Data
public class Imagen_mongodb_MySQLDTO {
    private String _id;
    private String imagen;
    private Personas persona;
}
