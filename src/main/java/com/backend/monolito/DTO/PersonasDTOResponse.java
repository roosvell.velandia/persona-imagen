package com.backend.monolito.DTO;

import com.backend.monolito.modelo.Personas;
import lombok.*;

import java.util.List;

@Setter
@Getter
@Data
public class PersonasDTOResponse {
        private Long id_persona;
        private Long cedula;
        private String nombres;
        private String apellidos;

        public PersonasDTOResponse(Long id_persona, Long cedula, String nombres, String apellidos) {
                this.id_persona = id_persona;
                this.cedula = cedula;
                this.nombres = nombres;
                this.apellidos = apellidos;
        }

        public PersonasDTOResponse() {

        }
}
