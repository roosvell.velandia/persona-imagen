package com.backend.monolito.DTO;

import lombok.*;

@Setter
@Getter
@Data
public class PersonasDTO {
    private Long cedula;
    private String nombres;
    private String apellidos;

    public PersonasDTO() {
    }

    public PersonasDTO(Long cedula, String nombres, String apellidos) {
        this.cedula = cedula;
        this.nombres = nombres;
        this.apellidos = apellidos;
    }
}
