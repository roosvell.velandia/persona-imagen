package com.backend.monolito.DTO;

import com.backend.monolito.modelo.Personas;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@Data
public class Imagen_MySQLDTO {
    private Long id_imagen;
    private String imagen;
    private Personas persona;
}
