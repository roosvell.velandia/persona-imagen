package com.backend.monolito.Exception;

public class NotFoundException extends RuntimeException {
    private final static String DESCRIPTION = "Bad Request Exception (404)";

    public NotFoundException(String detail) {
        super(DESCRIPTION + "  " + detail);
    }
}
