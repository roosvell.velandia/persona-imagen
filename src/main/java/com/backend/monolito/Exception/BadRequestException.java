package com.backend.monolito.Exception;

public class BadRequestException extends RuntimeException{
    private final static String DESCRIPTION = "Bad Request Exception (400)";
    public BadRequestException(String detail){
    super( DESCRIPTION + "  " + detail);
}
}
