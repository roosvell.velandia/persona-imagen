package com.backend.monolito.mapper;

import com.backend.monolito.DTO.Imagen_MySQLDTO;
import com.backend.monolito.DTO.Imagen_mongodbDTO;
import com.backend.monolito.DTO.Imagen_mongodb_MySQLDTO;
import com.backend.monolito.modelo.Imagenes;
import com.backend.monolito.modelo.Imagenes_mongodb;
import com.backend.monolito.modelo.Imagenes_mongodb_MySQL;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface ImagenMapper {
    ImagenMapper INSTANCE = Mappers.getMapper(ImagenMapper.class);

    Imagen_MySQLDTO ImagenToImagenMySQLDTO (Imagenes imagen);

    List<Imagen_MySQLDTO> ListImagenesToListImagenesMySQLDTO (List<Imagenes> listaimagenes);

    Imagen_mongodbDTO ImagenToImagenMongodbDTO (Imagenes_mongodb imagen);

    List<Imagen_mongodbDTO> ListImagenesToListImagenesMongodbDTO(List<Imagenes_mongodb> listaimagenes);

    Imagen_mongodb_MySQLDTO ImagenToImagenMongoMySQLDTO (Imagenes_mongodb_MySQL imagen);

    List<Imagen_mongodb_MySQLDTO> ListImagenesToListImagensMongoMySQLDTO (List<Imagenes_mongodb_MySQL> listaimagenes);
}
