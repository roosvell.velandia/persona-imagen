package com.backend.monolito.mapper;

import com.backend.monolito.DTO.PersonasDTO;
import com.backend.monolito.DTO.PersonasDTOResponse;
import com.backend.monolito.modelo.Personas;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(componentModel = "spring", injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PersonaMapper{
    PersonaMapper INSTANCE = Mappers.getMapper(PersonaMapper.class);

    Personas PersonasDTOtoPersonas (PersonasDTO personasDto);

    PersonasDTOResponse PersonastoPersonasDTOResponse(Personas personas);

    List<PersonasDTOResponse> ListPersonatoListPersonaDTO(List<Personas> listapersonas);

    Personas PersonaDTOResponseToPersonas (PersonasDTOResponse personasDTOResponse);

}
