package com.backend.monolito.repository;

import com.backend.monolito.modelo.Personas;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPersonas extends CrudRepository<Personas, Long> {
}
