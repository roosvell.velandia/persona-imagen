package com.backend.monolito.repository;

import com.backend.monolito.modelo.Imagenes;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IImagen extends CrudRepository<Imagenes, Long> {
}

