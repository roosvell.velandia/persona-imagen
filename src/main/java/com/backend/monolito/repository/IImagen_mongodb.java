package com.backend.monolito.repository;

import com.backend.monolito.modelo.Imagenes;
import com.backend.monolito.modelo.Imagenes_mongodb;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IImagen_mongodb extends MongoRepository<Imagenes_mongodb, String> {
}

