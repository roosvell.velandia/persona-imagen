package com.backend.monolito.repository;

import com.backend.monolito.modelo.Imagenes_mongodb_MySQL;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IImagen_mongodb_MySQL extends MongoRepository<Imagenes_mongodb_MySQL, String> {
}

