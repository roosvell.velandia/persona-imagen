package com.backend.monolito.controlador;

import com.backend.monolito.DTO.PersonasDTO;
import com.backend.monolito.DTO.PersonasDTOResponse;
import com.backend.monolito.Exception.ApiExceptionHandler;
import com.backend.monolito.Exception.ResourceNotFoundException;
import com.backend.monolito.servicio.IPersonasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.POST,RequestMethod.GET, RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api/personas")
public class PersonaControlador {

    @Autowired
    private IPersonasService personaservice;

    //Trae la lista completa de personas.
    @GetMapping("/")
    public ResponseEntity<List<PersonasDTOResponse>> getAllPersonas(){
        return (ResponseEntity<List<PersonasDTOResponse>>) ResponseEntity.ok().body(personaservice.getAllPersonas());
    }
    //Trae la información de una persona solamente con su Id.
    @GetMapping("/{id}")
    public ResponseEntity<PersonasDTOResponse> getPersonaById(@PathVariable Long id){
        try {
            return (ResponseEntity<PersonasDTOResponse>) ResponseEntity.ok().body(personaservice.getPersonaById(id));
        } catch (Exception ex) {
            ex.printStackTrace();
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }

    }
    // Crea una nueva persona pasandole todos sus datos.
    @PostMapping("/")
    public ResponseEntity<PersonasDTOResponse> crearPersona(@RequestBody PersonasDTO personadto){
        return ResponseEntity.ok().body(personaservice.crearPersona(personadto));
    }
    // Actualiza los datos de una persona a partir de su Id.
    @PutMapping("/{id}")
    public ResponseEntity<PersonasDTOResponse> updatePersona(@PathVariable Long id, @RequestBody PersonasDTO persona){
        return (ResponseEntity<PersonasDTOResponse>) ResponseEntity.ok().body(personaservice.updatePersona(persona, id));
    }
    // Elimina un registro de persona con su Id.
    @DeleteMapping("/{id}")
    public HttpStatus deletePersona(@PathVariable Long id){
        this.personaservice.deletePersona(id);
        return HttpStatus.OK;
    }


}
