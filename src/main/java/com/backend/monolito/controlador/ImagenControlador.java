package com.backend.monolito.controlador;

import com.backend.monolito.DTO.Imagen_MySQLDTO;
import com.backend.monolito.modelo.Imagenes;
import com.backend.monolito.modelo.Personas;
import com.backend.monolito.servicio.IImagenService;
import com.backend.monolito.servicio.IPersonasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.POST,RequestMethod.GET, RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api/imagen")
public class ImagenControlador {

    @Autowired
    private IImagenService imagenservice;
    //Trae la lista completa de imagenes.
    @GetMapping("/")
    public ResponseEntity<List<Imagen_MySQLDTO>> getAllImagenes(){
        return (ResponseEntity<List<Imagen_MySQLDTO>>) ResponseEntity.ok().body(imagenservice.getAllimagenes());
    }
    //Crea una nueva imagen
    @PostMapping("/")
    public ResponseEntity<Imagen_MySQLDTO> saveimage (@RequestParam("file")MultipartFile imagenfile, @RequestParam Long id_persona) throws IOException {

        return (ResponseEntity<Imagen_MySQLDTO>) ResponseEntity.ok().body(imagenservice.guardarimagen(imagenfile,id_persona));
    }
    // actualiza la imagen con el id
    @PutMapping("/{id}")
    public ResponseEntity<Imagen_MySQLDTO> updateimage(@RequestParam("file")MultipartFile imagenfile,@PathVariable Long id, @RequestParam Long id_persona) throws IOException {
        return (ResponseEntity<Imagen_MySQLDTO>) ResponseEntity.ok().body(imagenservice.updateimagen(imagenfile,id, id_persona));
    }

    //Trae la información de una imagen solamente con su Id.
    @GetMapping("/{id}")
    public ResponseEntity<Imagen_MySQLDTO> getimagenId(@PathVariable Long id){
        return (ResponseEntity<Imagen_MySQLDTO>) ResponseEntity.ok().body(imagenservice.getimagenById(id));
    }
    //Elimina una imagen con su id.
    @DeleteMapping("/{id}")
    public HttpStatus deleteimagen(@PathVariable Long id){
        this.imagenservice.deleteimagen(id);
        return HttpStatus.OK;
    }

}
