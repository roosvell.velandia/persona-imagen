package com.backend.monolito.controlador;

import com.backend.monolito.DTO.Imagen_mongodbDTO;
import com.backend.monolito.modelo.Imagenes_mongodb;
import com.backend.monolito.servicio.IImagenService;
import com.backend.monolito.servicio.IImagenService_mongodb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.POST,RequestMethod.GET, RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api/imagen_mongodb")
public class ImagenControlador_mongodb {

    @Autowired
    private IImagenService_mongodb imagenservice;
    //Trae la lista completa de Imagenes_mongodb.
    @GetMapping("/")
    public ResponseEntity<List<Imagen_mongodbDTO>> getAllImagenes_mongodb(){
        return (ResponseEntity<List<Imagen_mongodbDTO>>) ResponseEntity.ok().body(imagenservice.getAllimagenes_mongo());
    }
    //Crea una nueva imagen
    @PostMapping("/")
    public ResponseEntity<Imagen_mongodbDTO> saveimage (@RequestParam("file")MultipartFile imagenfile) throws IOException {
        return (ResponseEntity<Imagen_mongodbDTO>) ResponseEntity.ok().body(imagenservice.guardarimagen_mongo(imagenfile));
    }
    // actualiza la imagen con el id
    @PutMapping("/{id}")
    public ResponseEntity<Imagen_mongodbDTO> updateimage(@RequestParam("file")MultipartFile imagenfile,@PathVariable String id) throws IOException {
        return (ResponseEntity<Imagen_mongodbDTO>) ResponseEntity.ok().body(imagenservice.updateimagen_mongo(imagenfile,id));
    }

    //Trae la información de una imagen solamente con su Id.
    @GetMapping("/{id}")
    public ResponseEntity<Imagen_mongodbDTO> getimagenId(@PathVariable String id){
        return (ResponseEntity<Imagen_mongodbDTO>) ResponseEntity.ok().body(imagenservice.getimagenById_mongo(id));
    }
    //Elimina una imagen con su id.
    @DeleteMapping("/{id}")
    public HttpStatus deleteimagen(@PathVariable String id){
        this.imagenservice.deleteimagen_mongo(id);
        return HttpStatus.OK;
    }

}
