package com.backend.monolito.controlador;

import com.backend.monolito.DTO.Imagen_mongodb_MySQLDTO;
import com.backend.monolito.modelo.Imagenes_mongodb;
import com.backend.monolito.modelo.Imagenes_mongodb_MySQL;
import com.backend.monolito.servicio.IImagenService_mongodb;
import com.backend.monolito.servicio.IImagenService_mongodb_MySQL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@RestController
@CrossOrigin(origins = "*", methods = {RequestMethod.POST,RequestMethod.GET, RequestMethod.PUT,RequestMethod.DELETE})
@RequestMapping("/api/imagen_mongodb_MySQL")
public class ImagenControlador_mongodb_MySQL {

    @Autowired
    private IImagenService_mongodb_MySQL imagenservice;
    //Trae la lista completa de Imagenes_mongodb.
    @GetMapping("/")
    public ResponseEntity<List<Imagen_mongodb_MySQLDTO>> getAllImagenes_mongodb_MySQL(){
        return (ResponseEntity<List<Imagen_mongodb_MySQLDTO>>) ResponseEntity.ok().body(imagenservice.getAllimagenes_mongo_MySQL());
    }
    //Crea una nueva imagen
    @PostMapping("/")
    public ResponseEntity<Imagen_mongodb_MySQLDTO> saveimage (@RequestParam("file")MultipartFile imagenfile,@RequestParam Long id_persona) throws IOException {
        return (ResponseEntity<Imagen_mongodb_MySQLDTO>) ResponseEntity.ok().body(imagenservice.guardarimagen_mongo_MySQL(imagenfile,id_persona));
    }
    // actualiza la imagen con el id
    @PutMapping("/{id}")
    public ResponseEntity<Imagen_mongodb_MySQLDTO> updateimage(@RequestParam("file")MultipartFile imagenfile,@PathVariable String id,@RequestParam Long id_persona) throws IOException {
        return (ResponseEntity<Imagen_mongodb_MySQLDTO>) ResponseEntity.ok().body(imagenservice.updateimagen_mongo_MySQL(imagenfile,id, id_persona));
    }

    //Trae la información de una imagen solamente con su Id.
    @GetMapping("/{id}")
    public ResponseEntity<Imagen_mongodb_MySQLDTO> getimagenId(@PathVariable String id){
        return (ResponseEntity<Imagen_mongodb_MySQLDTO>) ResponseEntity.ok().body(imagenservice.getimagenById_mongo_MySQL(id));
    }
    //Elimina una imagen con su id.
    @DeleteMapping("/{id}")
    public HttpStatus deleteimagen(@PathVariable String id){
        this.imagenservice.deleteimagen_mongo_MySQL(id);
        return HttpStatus.OK;
    }

}
