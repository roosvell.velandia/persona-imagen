package com.backend.monolito.servicio;

import com.backend.monolito.DTO.PersonasDTO;
import com.backend.monolito.DTO.PersonasDTOResponse;
import com.backend.monolito.Exception.NotFoundException;
import com.backend.monolito.mapper.PersonaMapper;
import com.backend.monolito.repository.IPersonas;
import com.backend.monolito.modelo.Personas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.backend.monolito.Exception.ResourceNotFoundException;

import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class PersonasImpl implements IPersonasService {
    
    /*
            @Autowired inyección de dependncias para acceder a los metodos
            del repositorio dao IPersonas
            */
    @Autowired
    private IPersonas ipersona;

    @Autowired
    private PersonaMapper personaMapper;

    public PersonasImpl(IPersonas ipersona, PersonaMapper personaMapper) {
        this.ipersona = ipersona;
        this.personaMapper = personaMapper;
    }

    //Creación de una persona con .save
    @Override
    public PersonasDTOResponse crearPersona(PersonasDTO persona) {
        return personaMapper.PersonastoPersonasDTOResponse(ipersona.save(personaMapper.PersonasDTOtoPersonas(persona)));
    }

    //Actualización de una persona, buscando su id y en caso de encontrarlo de muevo uso .save
    @Override
    public PersonasDTOResponse updatePersona(PersonasDTO persona, Long id) {
        Optional<Personas> personadb= this.ipersona.findById(id);
        if (personadb.isPresent()) {
            Personas personaUpdate = personadb.get();
            personaUpdate.setId_persona(id);
            personaUpdate.setApellidos(persona.getApellidos());
            personaUpdate.setNombres(persona.getNombres());
            personaUpdate.setCedula(persona.getCedula());
            ipersona.save(personaUpdate);
            return personaMapper.PersonastoPersonasDTOResponse(personaUpdate);
        } else {
            throw new ResourceNotFoundException("Registro no encontrado" + id);
        }
    }

    //con findAll trae todos los registros.
    @Override
    public List<PersonasDTOResponse> getAllPersonas() {
        System.out.println("in service" + ipersona.findAll());
        return personaMapper.ListPersonatoListPersonaDTO((List<Personas>) ipersona.findAll());
    }

    //Con findById me trae solo el registro que se indica.
    @Override
    public PersonasDTOResponse getPersonaById(Long id) {
        Optional<Personas> personadb = this.ipersona.findById(id);
        if(((Optional<?>) personadb).isPresent()){
            //System.out.println("in service " + personaMapper.PersonastoPersonasDTOResponse(personadb.get()));
            return personaMapper.PersonastoPersonasDTOResponse(personadb.get());
        } else {
            throw new NotFoundException("Registro no encontrado" + id);
        }

    }
    // Elimino con .delete el registro solicitado.
    @Override
    public void deletePersona(Long id) {
        Optional<Personas> personadb = this.ipersona.findById(id);
        if(personadb.isPresent()){
            this.ipersona.delete(personadb.get());
        } else {
            throw new ResourceNotFoundException("Registro no encontrado" + id);
        }
    }

}
