package com.backend.monolito.servicio;

import com.backend.monolito.DTO.Imagen_MySQLDTO;
import com.backend.monolito.modelo.Imagenes;
import com.backend.monolito.modelo.Personas;
import org.springframework.web.multipart.MultipartFile;
import java.io.IOException;
import java.util.List;

public interface IImagenService {
    Imagen_MySQLDTO guardarimagen (MultipartFile imagefile, Long id_persona) throws IOException;
    Imagen_MySQLDTO updateimagen (MultipartFile imagefile, Long id, Long id_persona) throws IOException;
    List<Imagen_MySQLDTO> getAllimagenes ();
    Imagen_MySQLDTO getimagenById (Long id);
    void deleteimagen (Long id);
}