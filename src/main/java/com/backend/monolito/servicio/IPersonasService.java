package com.backend.monolito.servicio;

import com.backend.monolito.DTO.PersonasDTO;
import com.backend.monolito.DTO.PersonasDTOResponse;
import com.backend.monolito.modelo.Personas;

import java.util.List;

public interface IPersonasService {
    PersonasDTOResponse crearPersona (PersonasDTO persona);
    PersonasDTOResponse updatePersona (PersonasDTO persona, Long id);
    List<PersonasDTOResponse> getAllPersonas ();
    PersonasDTOResponse getPersonaById (Long id);
    void deletePersona (Long id);
}
