package com.backend.monolito.servicio;

import com.backend.monolito.DTO.Imagen_MySQLDTO;
import com.backend.monolito.Exception.ResourceNotFoundException;
import com.backend.monolito.mapper.ImagenMapper;
import com.backend.monolito.modelo.Personas;
import com.backend.monolito.repository.IImagen;
import com.backend.monolito.modelo.Imagenes;
import com.backend.monolito.repository.IPersonas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ImagenImpl implements IImagenService{
    /*
    @Autowired inyección de dependencias para acceder a los metodos
    del repositorio dao IPersonas
    */
    @Autowired
    private IImagen iImagen;

    @Autowired
    private IPersonas iPersona;

    @Autowired
    private ImagenMapper imagenMapper;

    //Creación de una imagen con .save
    @Override
    public Imagen_MySQLDTO guardarimagen(MultipartFile imagefile, Long id_persona) throws IOException {
        Imagenes image = new Imagenes();
        //convierto la imagen en un mapa de bits y lo transformo en base 64
        byte[] fileContent = imagefile.getBytes();
        String encodedString = Base64
                .getEncoder()
                .encodeToString(fileContent);
        image.setImagen(encodedString);
        // busco si existe la persona a la cual asigno la imagen.
        Optional<Personas> personadb = this.iPersona.findById(id_persona);
        if (personadb.isPresent()) {
            Personas personaupdate = personadb.get();
            // actualizo los datos en la imagen
            image.setPersona(personaupdate);
        } else {
            throw new ResourceNotFoundException("Persona no encontrada" + id_persona);
        }
        return imagenMapper.ImagenToImagenMySQLDTO(iImagen.save(image));
    }

    @Override
    public Imagen_MySQLDTO updateimagen(MultipartFile imagefile, Long id, Long id_persona) throws IOException {
        // busco si la persona y la imagen existen en las tablas con sus respectivos ids
        Optional<Personas> personadb = this.iPersona.findById(id_persona);
        Optional<Imagenes> imagendb= this.iImagen.findById(id);
        Imagenes imagenUpdate = imagendb.get();
        //convierto la imagen en un mapa de bits y lo transformo en base 64
        byte[] fileContent = imagefile.getBytes();
        String encodedString = Base64
                .getEncoder()
                .encodeToString(fileContent);
        imagenUpdate.setImagen(encodedString);
        if (imagendb.isPresent()) {
            if (personadb.isPresent()) {
                // si existe actualizo la persona en la imagen.
                Personas personaupdate = personadb.get();
                imagenUpdate.setPersona(personaupdate);
            } else {
                throw new ResourceNotFoundException("Persona no encontrada" + id_persona);
            }
            // guardo la nueva imagen
            iImagen.save(imagenUpdate);
            return imagenMapper.ImagenToImagenMySQLDTO(imagenUpdate);
        } else {
            throw new ResourceNotFoundException("Registro no encontrado" + id);
        }
    }
    //con findAll trae todos los registros.
    @Override
    public List<Imagen_MySQLDTO> getAllimagenes() {
        return imagenMapper.ListImagenesToListImagenesMySQLDTO((List<Imagenes>) iImagen.findAll());
    }
    //Con findById me trae solo el registro que se indica.
    @Override
    public Imagen_MySQLDTO getimagenById(Long id) {
        Optional<Imagenes> imagendb = this.iImagen.findById(id);
        if(((Optional<?>) imagendb).isPresent()){
            return imagenMapper.ImagenToImagenMySQLDTO(imagendb.get());
        } else {
            throw new ResourceNotFoundException("Registro no encontrado" + id);
        }
    }
    // Elimino con .delete el registro solicitado.
    @Override
    public void deleteimagen(Long id) {
        Optional<Imagenes> imagendb = this.iImagen.findById(id);
        if(((Optional<?>) imagendb).isPresent()){
            iImagen.delete(imagendb.get());
        } else {
            throw new ResourceNotFoundException("Registro no encontrado" + id);
        }
    }
}
