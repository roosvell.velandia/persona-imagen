package com.backend.monolito.servicio;

import com.backend.monolito.DTO.Imagen_mongodbDTO;
import com.backend.monolito.modelo.Imagenes_mongodb;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface IImagenService_mongodb {
    Imagen_mongodbDTO guardarimagen_mongo (MultipartFile imagefile) throws IOException;
    Imagen_mongodbDTO updateimagen_mongo (MultipartFile imagefile, String id) throws IOException;
    List<Imagen_mongodbDTO> getAllimagenes_mongo ();
    Imagen_mongodbDTO getimagenById_mongo (String id);
    void deleteimagen_mongo (String id);
}