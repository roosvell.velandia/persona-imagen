package com.backend.monolito.servicio;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.backend.monolito.DTO.Imagen_mongodb_MySQLDTO;
import com.backend.monolito.DTO.PersonasDTO;
import com.backend.monolito.DTO.PersonasDTOResponse;
import com.backend.monolito.Exception.ResourceNotFoundException;
import com.backend.monolito.mapper.ImagenMapper;
import com.backend.monolito.mapper.PersonaMapper;
import com.backend.monolito.modelo.Imagenes;
import com.backend.monolito.modelo.Imagenes_mongodb_MySQL;
import com.backend.monolito.modelo.Personas;
import com.backend.monolito.repository.IImagen_mongodb_MySQL;
import com.backend.monolito.repository.IPersonas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ImagenImpl_mongodb_MySQL implements IImagenService_mongodb_MySQL{
    /*
    @Autowired inyección de dependencias para acceder a los metodos
    del repositorio dao IImagenes_mongodb
    */
    @Autowired
    private IImagen_mongodb_MySQL iImagen_mongodb_mySQL;

    @Autowired
    private IPersonas iPersona;

    @Autowired
    private ImagenMapper imagenMapper;

    @Autowired
    private PersonaMapper personaMapper;


    @Override
    public Imagen_mongodb_MySQLDTO guardarimagen_mongo_MySQL(MultipartFile imagefile, Long id_persona) throws IOException {
        Imagenes_mongodb_MySQL image = new Imagenes_mongodb_MySQL();
        //convierto la imagen en un mapa de bits y lo transformo en base 64
        byte[] fileContent = imagefile.getBytes();
        String encodedString = Base64
                .getEncoder()
                .encodeToString(fileContent);
        image.setImagen(encodedString);
        Optional<Personas> personadb = this.iPersona.findById(id_persona);

        if (personadb.isPresent()) {
            PersonasDTOResponse personaupdate = personaMapper.PersonastoPersonasDTOResponse(personadb.get());
            // actualizo los datos en la imagen
            image.setPersona(personaMapper.PersonaDTOResponseToPersonas(personaupdate));
        } else {
            throw new ResourceNotFoundException("Persona no encontrada" + id_persona);
        }
        Imagenes_mongodb_MySQL imagennew = iImagen_mongodb_mySQL.save(image);
        return imagenMapper.ImagenToImagenMongoMySQLDTO(imagennew);
    }

    @Override
    public Imagen_mongodb_MySQLDTO updateimagen_mongo_MySQL(MultipartFile imagefile, String id, Long id_persona) throws IOException {
        // busco si la persona y la imagen existen en las tablas con sus respectivos ids

        Optional <Imagenes_mongodb_MySQL> imagendb= this.iImagen_mongodb_mySQL.findById(id);
        Imagenes_mongodb_MySQL imagenUpdate = imagendb.get();
        Optional<Personas> personadb = this.iPersona.findById(id_persona);
        // busco si existe la persona
        if (personadb.isPresent()) {
            PersonasDTOResponse personaupdate = personaMapper.PersonastoPersonasDTOResponse(personadb.get());
            // actualizo los datos en la imagen
            imagenUpdate.setPersona(personaMapper.PersonaDTOResponseToPersonas(personaupdate));
        } else {
            throw new ResourceNotFoundException("Persona no encontrada" + id_persona);
        }
        //convierto la imagen en un mapa de bits y lo transformo en base 64
        byte[] fileContent = imagefile.getBytes();
        String encodedString = Base64
                .getEncoder()
                .encodeToString(fileContent);
        imagenUpdate.setImagen(encodedString);
        //busco si existe la imagen
        if (imagendb.isPresent()) {
            // devuelvo la imagen actualizada.
            return imagenMapper.ImagenToImagenMongoMySQLDTO(iImagen_mongodb_mySQL.save(imagenUpdate));
        } else {
            throw new ResourceNotFoundException("Registro no encontrado" + id);
        }
    }

    @Override
    public List<Imagen_mongodb_MySQLDTO> getAllimagenes_mongo_MySQL() {
        return imagenMapper.ListImagenesToListImagensMongoMySQLDTO(iImagen_mongodb_mySQL.findAll());
    }

    @Override
    public Imagen_mongodb_MySQLDTO getimagenById_mongo_MySQL(String id) {
        Optional<Imagenes_mongodb_MySQL> imagendb = this.iImagen_mongodb_mySQL.findById(id);
        if(((Optional<?>) imagendb).isPresent()){
            return imagenMapper.ImagenToImagenMongoMySQLDTO(imagendb.get());
        } else {
            throw new ResourceNotFoundException("Registro no encontrado" + id);
        }
    }

    @Override
    public void deleteimagen_mongo_MySQL(String id) {
        Optional<Imagenes_mongodb_MySQL> imagendb = this.iImagen_mongodb_mySQL.findById(id);
        if(((Optional<?>) imagendb).isPresent()){
            iImagen_mongodb_mySQL.delete(imagendb.get());
        } else {
            throw new ResourceNotFoundException("Registro no encontrado" + id);
        }
    }
}
