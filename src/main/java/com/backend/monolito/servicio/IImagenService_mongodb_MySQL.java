package com.backend.monolito.servicio;
import com.backend.monolito.DTO.Imagen_mongodb_MySQLDTO;
import com.backend.monolito.modelo.Imagenes_mongodb_MySQL;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface IImagenService_mongodb_MySQL {
    Imagen_mongodb_MySQLDTO guardarimagen_mongo_MySQL (MultipartFile imagefile, Long id_persona) throws IOException;
    Imagen_mongodb_MySQLDTO updateimagen_mongo_MySQL (MultipartFile imagefile, String id, Long id_persona) throws IOException;
    List<Imagen_mongodb_MySQLDTO> getAllimagenes_mongo_MySQL ();
    Imagen_mongodb_MySQLDTO getimagenById_mongo_MySQL (String id);
    void deleteimagen_mongo_MySQL (String id);
}