package com.backend.monolito.servicio;

import com.backend.monolito.DTO.Imagen_mongodbDTO;
import com.backend.monolito.Exception.ResourceNotFoundException;
import com.backend.monolito.mapper.ImagenMapper;
import com.backend.monolito.modelo.Imagenes_mongodb;
import com.backend.monolito.repository.IImagen_mongodb;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.Base64;
import java.util.List;
import java.util.Optional;

@Service
@Transactional
public class ImagenImpl_mongodb implements IImagenService_mongodb{
    /*
    @Autowired inyección de dependencias para acceder a los metodos
    del repositorio dao IImagenes_mongodb
    */
    @Autowired
    private IImagen_mongodb iImagen_m;

    @Autowired
    private ImagenMapper imagenMapper;


    @Override
    public Imagen_mongodbDTO guardarimagen_mongo(MultipartFile imagefile) throws IOException {
        Imagenes_mongodb imagen = new Imagenes_mongodb();
        //convierto la imagen en un mapa de bits y lo transformo en base 64
        byte[] fileContent = imagefile.getBytes();
        String encodedString = Base64
                .getEncoder()
                .encodeToString(fileContent);
        imagen.setImagen(encodedString);
        return imagenMapper.ImagenToImagenMongodbDTO(iImagen_m.save(imagen));
    }

    @Override
    public Imagen_mongodbDTO updateimagen_mongo(MultipartFile imagefile, String id) throws IOException {
        // busco si la persona y la imagen existen en las tablas con sus respectivos ids
        Optional<Imagenes_mongodb> imagendb= this.iImagen_m.findById(id);
        Imagenes_mongodb imagenUpdate = imagendb.get();
        //convierto la imagen en un mapa de bits y lo transformo en base 64
        byte[] fileContent = imagefile.getBytes();
        String encodedString = Base64
                .getEncoder()
                .encodeToString(fileContent);
        imagenUpdate.setImagen(encodedString);
        if (imagendb.isPresent()) {
            return imagenMapper.ImagenToImagenMongodbDTO(iImagen_m.save(imagenUpdate));
        } else {
            throw new ResourceNotFoundException("Registro no encontrado" + id);
        }
    }

    @Override
    public List<Imagen_mongodbDTO> getAllimagenes_mongo() {

        return imagenMapper.ListImagenesToListImagenesMongodbDTO(iImagen_m.findAll());
    }

    @Override
    public Imagen_mongodbDTO getimagenById_mongo(String id) {
        Optional<Imagenes_mongodb> imagendb = this.iImagen_m.findById(id);
        if(((Optional<?>) imagendb).isPresent()){
            return imagenMapper.ImagenToImagenMongodbDTO(imagendb.get());
        } else {
            throw new ResourceNotFoundException("Registro no encontrado" + id);
        }
    }

    @Override
    public void deleteimagen_mongo(String id) {
        Optional<Imagenes_mongodb> imagendb = this.iImagen_m.findById(id);
        if(((Optional<?>) imagendb).isPresent()){
            iImagen_m.delete(imagendb.get());
        } else {
            throw new ResourceNotFoundException("Registro no encontrado" + id);
        }
    }
}
