package com.backend.monolito.datos;

import com.backend.monolito.DTO.PersonasDTOResponse;
import com.backend.monolito.modelo.Imagenes;
import com.backend.monolito.modelo.Personas;
import lombok.*;

import java.util.*;

@Data
@Getter
@Setter
public class datosPersonas {
    public static List<Personas> crearpersonas() {
        return Arrays.asList(new Personas(1L, 12345L, "roosvell", "Velandia"),
                new Personas(2L, 54321L, "Andrea", "Jimenez"));
    }

    public static final Optional<Personas> crearpersona1(){
        return Optional.of(new Personas (1L, 12345L, "roosvell","Velandia"));
    }

    public static final Optional<Personas> crearpersona2() {
        return Optional.of(new Personas(2L, 54321L, "Andrea", "Jimenez"));
    }

    public static final Optional<Personas> crearpersona3() {
        return Optional.of(new Personas(3L, 11111L, "Rafael", "suarez"));
    }

}
