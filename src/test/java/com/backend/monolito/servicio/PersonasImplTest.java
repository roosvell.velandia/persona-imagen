package com.backend.monolito.servicio;

import com.backend.monolito.DTO.PersonasDTOResponse;
import com.backend.monolito.datos.datosPersonas;
import com.backend.monolito.mapper.PersonaMapper;
import com.backend.monolito.repository.IPersonas;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
class PersonasImplTest {
    @MockBean
    IPersonas repositorio;

    @SpyBean
    PersonaMapper mapper;

    @Autowired
    PersonasImpl servicio;

    @Test
    void crearPersona() {
    }

    @Test
    void updatePersona() {
    }

    @Test
    void getAllPersonas() {
        when(repositorio.findAll()).thenReturn(datosPersonas.crearpersonas());
        when(repositorio.findById(1L)).thenReturn(datosPersonas.crearpersona1());
        when(repositorio.findById(2L)).thenReturn(datosPersonas.crearpersona2());
        List<PersonasDTOResponse> personas = servicio.getAllPersonas();
        for (PersonasDTOResponse persona:personas){
            servicio.getPersonaById(persona.getId_persona());
            assertNotNull(persona);
        }
        verify(repositorio).findById(1L);
        verify(repositorio).findById(2L);
        verify(repositorio,times(2)).findById(any(Long.class));
        verify(repositorio,times(2)).findAll();
    }

    @Test
    void getPersonaById() {
        when(repositorio.findById(1L)).thenReturn(datosPersonas.crearpersona1());
        when(repositorio.findById(2L)).thenReturn(datosPersonas.crearpersona2());

        PersonasDTOResponse persona1 = servicio.getPersonaById(1L);
        PersonasDTOResponse persona2 = servicio.getPersonaById(2L);
        //PersonasDTOResponse persona3 = servicio.getPersonaById(3L);


        assertEquals(1L,persona1.getId_persona());
        assertEquals(2L,persona2.getId_persona());

        verify(repositorio, times(1)).findById(1L);
        verify(repositorio, times(1)).findById(2L);
        assertThrows(RuntimeException.class, ()->{
            servicio.getPersonaById(3L);
        });
        verify(repositorio, times(3)).findById(any(Long.class));
    }

    @Test
    void deletePersona() {
    }
}